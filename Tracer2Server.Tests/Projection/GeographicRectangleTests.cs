﻿using NUnit.Framework;
using Tracer2Server.Projection;

namespace Tracer2Server.Tests.Projection
{
    class GeographicRectangleTests
    {
        [Test]
        public void Constructor_DimensionsGiven_PropertiesProperlySet()
        {
            var rectangle = new GeographicRectangle(0, 1, 1, 0);

            Assert.That(rectangle.Bottom, Is.EqualTo(0));
            Assert.That(rectangle.Top, Is.EqualTo(1));
            Assert.That(rectangle.Left, Is.EqualTo(0));
            Assert.That(rectangle.Right, Is.EqualTo(1));
        }

        [Test]
        public void Constructor_PointsGiven_PropertiesProperlySet()
        {
            var topLeft = new GeographicPoint(1, 0);
            var bottomRight = new GeographicPoint(0, 1);

            var rectangle = new GeographicRectangle(topLeft, bottomRight);

            Assert.That(rectangle.Bottom, Is.EqualTo(0));
            Assert.That(rectangle.Top, Is.EqualTo(1));
            Assert.That(rectangle.Left, Is.EqualTo(0));
            Assert.That(rectangle.Right, Is.EqualTo(1));
        }
    }
}
