﻿using NUnit.Framework;
using Tracer2Server.Projection;

namespace Tracer2Server.Tests.Projection
{
    class GeographicPointTests
    {
        [Test]
        public void Constructor_SetsProperties()
        {
            var point = new GeographicPoint(10.0, 20.1);

            Assert.That(point.Latitude, Is.EqualTo(10.0));
            Assert.That(point.Longitude, Is.EqualTo(20.1));
        }

        [Test]
        public void ExplicitConversionOperator_GivenPointD_ReturnsPointGeoWithPropertiesSet()
        {
            var pointD = new CarthesianPoint(2, 3);

            GeographicPoint geo = (GeographicPoint)pointD;

            Assert.That(geo.Latitude, Is.EqualTo(pointD.X));
            Assert.That(geo.Longitude, Is.EqualTo(pointD.Y));
        }
    }
}
