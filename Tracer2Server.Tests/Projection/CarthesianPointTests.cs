﻿using NUnit.Framework;
using System.Drawing;
using Tracer2Server.Projection;

namespace Tracer2Server.Tests.Projection
{
    class CarthesianPointTests
    {
        [Test]
        public void Constructor_SetsProperties()
        {
            var point = new CarthesianPoint(10.0, 20.1);

            Assert.That(point.X, Is.EqualTo(10.0));
            Assert.That(point.Y, Is.EqualTo(20.1));
        }

        [Test]
        public void PlusOperator_TwoPoints_ProperCarthesianAddition()
        {
            var point1 = new CarthesianPoint(1, 0);
            var point2 = new CarthesianPoint(0, 1);

            var result = point1 + point2;

            Assert.That(result.X, Is.EqualTo(1));
            Assert.That(result.Y, Is.EqualTo(1));
        }

        [Test]
        public void PlusOperator_OneDrawingPoint_OnePointD_ProperCarthesianAddition()
        {
            var point1 = new CarthesianPoint(1, 0);
            var point2 = new Point(0, 1);

            var result = point1 + point2;

            Assert.That(result.X, Is.EqualTo(1));
            Assert.That(result.Y, Is.EqualTo(1));
        }
    }
}
