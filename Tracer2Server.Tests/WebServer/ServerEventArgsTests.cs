﻿using NUnit.Framework;
using Tracer2Server.WebServer;

namespace Tracer2Server.Tests.WebServer
{
    [TestFixture]
    public class ServerEventArgsTests
    {
        [Test]
        [Ignore("Need to add validation.")]
        public void SetArgs_EmptyString()
        {
            var args = new ServerEventArgs();

            var result = args.SetArgs(string.Empty);

            //TODO: currently result is true
            Assert.That(result, Is.False);
        }

        [Test]
        public void SetArgs_ProperlyFormattedString_SetsProperties()
        {
            var input = "traceOrder=&traceLat=1&traceLon=2&traceName=Test&traceUrl=test.url&traceTileSize=0.2&traceResolution=1024&traceSkipBottom=1&traceMode=test&traceThreshold=126&tracePointsPerCircle=12";
            var args = new ServerEventArgs();

            var result = args.SetArgs(input);

            Assert.That(result, Is.True);
            Assert.That(args.Latitude, Is.EqualTo(1));
            Assert.That(args.Longitude, Is.EqualTo(2));
            Assert.That(args.Name, Is.EqualTo("Test"));
            Assert.That(args.Url, Is.EqualTo("test.url"));
            Assert.That(args.TileSize, Is.EqualTo(0.2));
            Assert.That(args.Resolution, Is.EqualTo(1024));
            Assert.That(args.SkipBottom, Is.EqualTo(1));
            Assert.That(args.Mode, Is.EqualTo("test"));
            Assert.That(args.IsModeBoundary, Is.False);
            Assert.That(args.Threshold, Is.EqualTo(126));
            Assert.That(args.PointsPerCircle, Is.EqualTo(12));
        }

        [Test]
        public void SetArgs_LatitudeGreaterThan90_ReturnsFalse()
        {
            var input = "traceOrder=&traceLat=91";
            var args = new ServerEventArgs();

            var result = args.SetArgs(input);

            Assert.That(result, Is.False);
        }

        [Test]
        public void SetArgs_LongitudeGreaterThan180_ReturnsFalse()
        {
            var input = "traceOrder=&traceLon=181";
            var args = new ServerEventArgs();

            var result = args.SetArgs(input);

            Assert.That(result, Is.False);
        }
    }
}