﻿using NUnit.Framework;
using Tracer2Server.WebServer;

namespace Tracer2Server.Tests.WebServer
{
    [TestFixture]
    public class ServerTests
    {
        [Test]
        public void ServerTest()
        {
            var server = new Server(1);

            Assert.That(server.Port, Is.EqualTo(1));
        }
    }
}