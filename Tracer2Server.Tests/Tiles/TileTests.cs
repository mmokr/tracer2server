﻿using NUnit.Framework;
using Tracer2Server.Projection;
using System.Drawing;
using Tracer2Server.Tiles;

namespace Tracer2Server.Tests.Tiles
{
    [TestFixture]
    public class TileTests
    {
        [Test]
        public void TileTest()
        {
            var rectangle = new GeographicRectangle(0, 1, 1, 0);
            var firstPoint = new Point(0, 0);
            var tile = new Tile(rectangle, firstPoint, 1);

            Assert.That(tile.Rectangle, Is.EqualTo(rectangle));
            Assert.That(tile.FirstPointXY, Is.EqualTo(firstPoint));
            Assert.That(tile.Resolution, Is.EqualTo(1));
        }

        [Test]
        public void CloneTest()
        {
            var tile = GetDefaultTile();

            var clone = tile.Clone();

            Assert.That(tile, Is.Not.EqualTo(clone));
            Assert.That(tile.Resolution, Is.EqualTo(clone.Resolution));
            Assert.That(tile.FirstPointXY, Is.EqualTo(clone.FirstPointXY));
            Assert.That(tile.Rectangle, Is.EqualTo(clone.Rectangle));
        }

        private Tile GetDefaultTile()
        {
            var rectangle = new GeographicRectangle(0, 1, 1, 0);
            var firstPoint = new Point(0, 0);
            return new Tile(rectangle, firstPoint, 1);
        }
    }
}