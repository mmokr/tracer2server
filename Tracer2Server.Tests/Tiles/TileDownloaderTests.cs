﻿using NUnit.Framework;
using Tracer2Server.Tiles;
using System;
using Tracer2Server.WebServer;

namespace Tracer2Server.Tests.Tiles
{
    [TestFixture]
    public class TileDownloaderTests
    {
        [Test]
        public void Constructor_EmptyArgs_ThrowsNullReferenceException()
        {
            var serverArgs = new ServerEventArgs();
            Assert.Throws<NullReferenceException>(() => new TileDownloader(serverArgs));
        }
    }
}