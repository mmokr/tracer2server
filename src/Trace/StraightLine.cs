﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System;
using System.Globalization;
using Tracer2Server.Projection;

namespace Tracer2Server.Trace
{
    public struct StraightLine
    {
        public StraightLine(double slope, double yIntercept, bool revert)
        {
            Slope = slope;
            YIntercept = yIntercept;
            XIntercept = double.NaN;
            if (double.IsNaN(Slope))
            {
                Alpha = double.NaN;
            }
            else if (double.IsInfinity(Slope))
            {
                Alpha = CheckAlpha(double.IsNegativeInfinity(Slope) ? -90 : 90);
            }
            else
            {
                Alpha = CheckAlpha(Math.Atan(slope) * 180 / Math.PI + (revert ? 180 : 0));
            }
        }

        public StraightLine(CarthesianPoint oP1, CarthesianPoint oP2)
        {
            Slope = (oP2.Y - oP1.Y) / (oP2.X - oP1.X);
            YIntercept = oP1.Y - (Slope * oP1.X);

            if (double.IsInfinity(Slope) || double.IsNaN(Slope))
            {
                XIntercept = oP1.X;
                Alpha = CheckAlpha(oP1.Y > oP2.Y ? -90 : 90);
            }
            else
            {
                XIntercept = double.NaN;
                Alpha = CheckAlpha(Math.Atan((oP2.Y - oP1.Y) / (oP2.X - oP1.X)) * 180 / Math.PI + (oP1.X > oP2.X ? 180 : 0));
            }
        }

        static public double CheckAlpha(double dAlpha)
        {
            if (dAlpha > 180)
            {
                return dAlpha - 360;
            }
            if (dAlpha <= -180)
            {
                return dAlpha + 360;
            }
            return dAlpha;
        }

        /// <summary>
        /// Slope (gradient)
        /// </summary>
        public double Slope { get; }

        /// <summary>
        /// Y-intercept
        /// </summary>
        public double YIntercept { get; }

        /// <summary>
        /// x-intercept (if y-intercept == NaN)
        /// </summary>
        public double XIntercept { get; }

        /// <summary>
        /// Alpha (-180° < alpha <= 180°)
        /// </summary>
        public double Alpha { get; }

        public bool IsLine
        {
            get
            {
                return !(double.IsNaN(XIntercept) || double.IsInfinity(XIntercept) || double.IsNaN(Alpha) || double.IsInfinity(Alpha))
                    || !(double.IsNaN(Slope) || double.IsInfinity(Slope) || double.IsNaN(YIntercept) || double.IsInfinity(YIntercept));
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator ==(StraightLine oLine1, StraightLine oLine2)
        {
            return
                oLine1.Slope == oLine2.Slope &&
                oLine1.YIntercept == oLine2.YIntercept &&
                oLine1.XIntercept == oLine2.XIntercept &&
                oLine1.Alpha == oLine2.Alpha;
        }

        public static bool operator !=(StraightLine oLine1, StraightLine oLine2)
        {
            return !(oLine1 == oLine2);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is StraightLine)) return false;
            StraightLine oLineD = (StraightLine)obj;
            return
                oLineD.Slope == Slope &&
                oLineD.YIntercept == YIntercept &&
                oLineD.XIntercept == XIntercept &&
                oLineD.Alpha == Alpha &&
                oLineD.GetType().Equals(GetType());
        }

        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture, "m={0:F3},	c={1:F3}, alpha={2:F3},	x={3:F3}", Slope, YIntercept, Alpha, XIntercept);
        }

        public CarthesianPoint GetIntersectionPoint(StraightLine line)
        {
            double dx;
            double dy;

            if (!IsLine || !line.IsLine)
            {
                // data missing
                return new CarthesianPoint(double.NaN, double.NaN);
            }

            if (double.IsInfinity(Slope) || double.IsNaN(Slope))
            {
                if (double.IsInfinity(line.Slope) || double.IsNaN(line.Slope))
                {
                    // no IntersectionPoint
                    return new CarthesianPoint(double.NaN, double.NaN);
                }
                else
                {
                    dx = XIntercept;
                    dy = line.Slope * dx + line.YIntercept;
                }
            }
            else if (double.IsInfinity(line.Slope) || double.IsNaN(line.Slope))
            {
                dx = line.XIntercept;
                dy = Slope * dx + YIntercept;
            }
            else
            {
                dx = (line.YIntercept - YIntercept) / (Slope - line.Slope);
                dy = Slope * dx + YIntercept;
            }
            return new CarthesianPoint(dx, dy);
        }
    }
}
