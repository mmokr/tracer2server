﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using Tracer2Server.Projection;
using Tracer2Server.Debug;
using Tracer2Server.Trace.Fit;

namespace Tracer2Server.Trace
{
    internal class ShapeFinder
    {
        private const byte AREA = 0;
        private const byte BORDER = 1;

        private List<Point> BorderPoints;
        private LinkedList<BorderPart> BorderLines;

        private readonly TracerParam _tracerParam;

        public ShapeFinder(TracerParam oTracerParam)
        {
            _tracerParam = oTracerParam;
        }

        public void GetStartPoint()
        {
            int positionX = _tracerParam.InnerPointXY.X;
            int positionY = _tracerParam.InnerPointXY.Y;

            if (_tracerParam.GeoMap.GetPix(positionX, positionY) == BORDER)
            {
                throw new Exception("Can't find sarting point");
            }
            do
            {
                positionY += 1;
            }
            while (_tracerParam.GeoMap.GetPix(positionX, positionY) == AREA);
            Point startPoint = new Point(positionX, positionY);

            LinkedList<BorderPart> borderLines = new LinkedList<BorderPart>();
            borderLines.AddLast(new BorderPartLine(_tracerParam.InnerPointXY, startPoint));
            _tracerParam.SetBorderLine(borderLines.ToList(), "Find starting point");

            _tracerParam.SetStartPoint(startPoint);
        }

        public void GetBorderPoints()
        {
            BorderPoints = new List<Point>(60000);

            GetPoints(1);

            _tracerParam.SetBorderPoints(BorderPoints);

            _tracerParam.ShapeAreaXY = GetShapeArea();

            DebugStatic.oDebugBitmap.GeoMap = _tracerParam.GeoMap;
            DebugStatic.oDebugBitmap.MakeBitmap(_tracerParam.InnerPointXY, _tracerParam.ShapeAreaXY);
        }

        private Rectangle GetShapeArea()
        {
            int xmin = BorderPoints[0].X;
            int xmax = BorderPoints[0].X;
            int ymin = BorderPoints[0].Y;
            int ymax = BorderPoints[0].Y;

            foreach (Point point in BorderPoints)
            {
                if (xmin > point.X)
                {
                    xmin = point.X;
                }
                else if (xmax < point.X)
                {
                    xmax = point.X;
                }
                if (ymin > point.Y)
                {
                    ymin = point.Y;
                }
                else if (ymax < point.Y)
                {
                    ymax = point.Y;
                }
            }
            return new Rectangle(xmin, ymin, xmax - xmin, ymax - ymin);
        }

        private void GetPoints(int nRotation)
        {
            int direction = (int)DIRECTION_XY.DOWN;
            int startPositionX = _tracerParam.StartPointXY.X;
            int startPositionY = _tracerParam.StartPointXY.Y;
            int currentPositionX = startPositionX;
            int currentPositionY = startPositionY;
            byte ucData;
            int count;
            do
            {
                BorderPoints.Add(new Point(currentPositionX, currentPositionY));
                count = 9;
                do
                {
                    if (count-- == 0)
                    {
                        return;
                    }
                    direction = (direction + nRotation) & 0x7;
                    ucData = _tracerParam.GeoMap.GetPix(currentPositionX + MyDirection.m_aoDirX[direction], currentPositionY + MyDirection.m_aoDirY[direction]);
                } while (ucData == AREA);
                currentPositionX += MyDirection.m_aoDirX[direction];
                currentPositionY += MyDirection.m_aoDirY[direction];
                direction = (direction - (3 * nRotation)) & 0x7;
            } while (BorderPoints.Count < 100000 && ((currentPositionX != startPositionX) || (currentPositionY != startPositionY)));
        }

        public void GetBorderLine()
        {
            string currentStep;
            int turns;

            BorderPartLine line;
            BorderPartLine newLine;
            Point startingPoint;
            LinkedListNode<BorderPart> oLineNodeStart;
            LinkedListNode<BorderPart> oLineNode;
            LinkedListNode<BorderPart> oLineNode1;
            LinkedListNode<BorderPart> oLineNode2;
            LinkedListNode<BorderPart> oLineNodeBack1;
            LinkedListNode<BorderPart> oLineNodeBack2;

            BorderLines = new LinkedList<BorderPart>();

            currentStep = "Combine points with same direction";
            DebugStatic.oDebugWatch.StartWatch(currentStep);
            Point oPointEnd;
            startingPoint = BorderPoints[BorderPoints.Count - 1];
            oPointEnd = BorderPoints[0];
            line = new BorderPartLine(startingPoint, oPointEnd);
            startingPoint = oPointEnd;
            turns = BorderPoints.Count;
            for (int i = 1; i < turns; i++)
            {
                oPointEnd = BorderPoints[i];
                newLine = new BorderPartLine(startingPoint, oPointEnd);
                if (line.CombineSameLine1(newLine))
                {
                    BorderLines.AddLast(line);
                    line = newLine;
                }
                startingPoint = oPointEnd;
            }
            if (((BorderPartLine)BorderLines.First.Value).CombineSameLine1(line))
            {
                BorderLines.AddLast(line);
            }
            DebugStatic.oDebugWatch.StopWatch(currentStep, BorderLines.Count().ToString());
            DebugStatic.oDebugBitmap.MakeBitmap(currentStep, BorderLines.ToArray());
            _tracerParam.SetBorderLine(BorderLines.ToList(), currentStep);

            if (BorderLines.Count < 3)
            {
                // no area
                _tracerParam.Error = "No area found.";
                BorderLines = new LinkedList<BorderPart>();
                return;
            }

            // calc line
            foreach (BorderPart oBorderPart in BorderLines)
            {
                ((BorderPartLine)oBorderPart).CalcLine();
            }

            currentStep = "Split corner line";
            DebugStatic.oDebugWatch.StartWatch(currentStep);
            oLineNode = BorderLines.First;
            turns = BorderLines.Count;
            for (int i = 0; i < turns * 2; i++)
            {
                if (((BorderPartLine)oLineNode.Value).m_nSize > 2)
                {
                    oLineNode1 = GetNextNode(oLineNode);
                    oLineNode2 = GetNextNode(oLineNode1);
                    oLineNodeBack1 = GetPreviousNode(oLineNode);
                    oLineNodeBack2 = GetPreviousNode(oLineNodeBack1);

                    if (((BorderPartLine)oLineNode1.Value).m_nSize == 2 && ((BorderPartLine)oLineNodeBack1.Value).m_nSize == 2 && ((BorderPartLine)oLineNode2.Value).m_nSize > 2 && ((BorderPartLine)oLineNodeBack2.Value).m_nSize > 2)
                    {
                        double dA1 = ((BorderPartLine)oLineNode.Value).GetDiffAlphaAbs((BorderPartLine)oLineNode2.Value);
                        double dA2 = ((BorderPartLine)oLineNode.Value).GetDiffAlphaAbs((BorderPartLine)oLineNodeBack2.Value);
                        double dA3 = ((BorderPartLine)oLineNode1.Value).GetDiffAlphaAbs((BorderPartLine)oLineNodeBack1.Value);
                        if (dA1 < 1 && dA2 < 1 && dA3 > 80 && dA3 < 100)
                        {
                            int nPos1 = BorderPoints.IndexOf(((BorderPartLine)oLineNode.Value).m_oPointStart);
                            int nPos2 = BorderPoints.IndexOf(((BorderPartLine)oLineNode.Value).m_oPointEnd);
                            //int nCount = 1 + (int)( (double)((((BorderPartLine)oLineNode.Value).m_nSize - 3) * nPos1 / (nPos1 + nPos2)));

                            //nPos1 = nPos1 + nCount;
                            nPos1++;
                            if (nPos1 >= BorderPoints.Count)
                            {
                                nPos1 -= BorderPoints.Count;
                            }

                            //nPos2 = nPos1 + 1;
                            nPos2--;

                            if (nPos2 < 0)
                            {
                                nPos2 += BorderPoints.Count;
                            }

                            BorderLines.AddAfter(oLineNode, ((BorderPartLine)oLineNode.Value).SplitLine(BorderPoints[nPos1], BorderPoints[nPos2]));
                            oLineNode = oLineNodeBack1;
                        }
                    }
                }
                oLineNode = GetNextNode(oLineNode);
            }
            DebugStatic.oDebugWatch.StopWatch(currentStep, BorderLines.Count().ToString());
            DebugStatic.oDebugBitmap.MakeBitmap(currentStep, BorderLines.ToArray());
            _tracerParam.SetBorderLine(BorderLines.ToList(), currentStep);


            currentStep = "Combine line with same length";
            DebugStatic.oDebugWatch.StartWatch(currentStep);
            oLineNode = BorderLines.First;
            turns = BorderLines.Count();
            for (int i = 0; i < turns; i++)
            {
                int nTemp = ((BorderPartLine)oLineNode.Value).m_nSize;
                oLineNodeBack1 = GetPreviousNode(oLineNode);
                oLineNode1 = GetNextNode(oLineNode);
                oLineNode2 = GetNextNode(oLineNode1);
                while (((BorderPartLine)oLineNode2.Value).m_nSize == nTemp && (((BorderPartLine)oLineNodeBack1.Value).m_oDirection == ((BorderPartLine)oLineNode1.Value).m_oDirection) && (((BorderPartLine)oLineNode.Value).CombineSameLine((BorderPartLine)oLineNode1.Value, (BorderPartLine)oLineNode2.Value) == true))
                {
                    BorderLines.Remove(oLineNode1.Value);
                    BorderLines.Remove(oLineNode2.Value);
                    oLineNode1 = GetNextNode(oLineNode);
                    oLineNode2 = GetNextNode(oLineNode1);
                }

                if (i < 2)
                {
                    oLineNodeBack1 = GetNextNode(oLineNode);
                    oLineNode1 = GetPreviousNode(oLineNode);
                    oLineNode2 = GetPreviousNode(oLineNode1);
                    while (((BorderPartLine)oLineNode2.Value).m_nSize == nTemp && (((BorderPartLine)oLineNodeBack1.Value).m_oDirection == ((BorderPartLine)oLineNode1.Value).m_oDirection) && (((BorderPartLine)oLineNode.Value).CombineSameLine((BorderPartLine)oLineNode1.Value, (BorderPartLine)oLineNode2.Value) == true))
                    {
                        BorderLines.Remove(oLineNode1.Value);
                        BorderLines.Remove(oLineNode2.Value);
                        oLineNode1 = GetPreviousNode(oLineNode);
                        oLineNode2 = GetPreviousNode(oLineNode1);
                    }
                }
                oLineNode = GetNextNode(oLineNode);
            }
            DebugStatic.oDebugWatch.StopWatch(currentStep, BorderLines.Count().ToString());
            DebugStatic.oDebugBitmap.MakeBitmap(currentStep, BorderLines.ToArray());
            _tracerParam.SetBorderLine(BorderLines.ToList(), currentStep);


            //for (int s = 2; s < 5; s++)
            for (int s = 5; s > 1; s--)
            {
                currentStep = "Combine line with same alpha (size=" + s + ")";
                DebugStatic.oDebugWatch.StartWatch(currentStep);
                oLineNode = BorderLines.First;
                turns = BorderLines.Count;
                for (int i = 0; i < turns; i++)
                {
                    oLineNode1 = GetNextNode(oLineNode);
                    oLineNode2 = GetNextNode(oLineNode1);
                    if (((BorderPartLine)oLineNode1.Value).m_nSize == s)
                    {
                        if (((BorderPartLine)oLineNode1.Value).IsSameAlpha((BorderPartLine)oLineNode.Value) && ((BorderPartLine)oLineNode1.Value).IsSameAlpha((BorderPartLine)oLineNode2.Value))
                        {
                            double dA1 = ((BorderPartLine)oLineNode1.Value).GetDiffAlphaAbs((BorderPartLine)oLineNode.Value);
                            double dA2 = ((BorderPartLine)oLineNode1.Value).GetDiffAlphaAbs((BorderPartLine)oLineNode2.Value);
                            if (dA1 < dA2 || (dA1 == dA2 && ((BorderPartLine)oLineNode.Value).m_nSize > ((BorderPartLine)oLineNode2.Value).m_nSize))
                            {
                                if (((BorderPartLine)oLineNode.Value).IsSameAlpha((BorderPartLine)oLineNode1.Value) == true)
                                {
                                    if (((BorderPartLine)oLineNode.Value).CombineLine((BorderPartLine)oLineNode1.Value))
                                    {
                                        BorderLines.Remove(oLineNode1.Value);
                                    }
                                }
                            }
                            else
                            {
                                if (((BorderPartLine)oLineNode1.Value).IsSameAlpha((BorderPartLine)oLineNode2.Value) == true)
                                {
                                    if (((BorderPartLine)oLineNode1.Value).CombineLine((BorderPartLine)oLineNode2.Value))
                                    {
                                        BorderLines.Remove(oLineNode2.Value);
                                    }
                                }
                            }
                        }
                    }
                    oLineNode = GetNextNode(oLineNode);
                }
                DebugStatic.oDebugWatch.StopWatch(currentStep, BorderLines.Count().ToString());
                DebugStatic.oDebugBitmap.MakeBitmap(currentStep, BorderLines.ToArray());
                _tracerParam.SetBorderLine(BorderLines.ToList(), currentStep);
            }

            for (int s = 4; s > 1; s--)
            {
                currentStep = "Combine (size=" + s + ")";
                DebugStatic.oDebugWatch.StartWatch(currentStep);
                oLineNode = BorderLines.First;
                turns = BorderLines.Count() * 4;
                for (int i = 0; i < turns; i++)
                {
                    oLineNode1 = GetNextNode(oLineNode);
                    oLineNode2 = GetNextNode(oLineNode1);
                    if (((BorderPartLine)oLineNode.Value).m_nSize > s && ((BorderPartLine)oLineNode.Value).m_nSize > ((BorderPartLine)oLineNode1.Value).m_nSize && ((BorderPartLine)oLineNode2.Value).m_nSize > s && ((BorderPartLine)oLineNode2.Value).m_nSize > ((BorderPartLine)oLineNode1.Value).m_nSize)
                    {
                        if (((BorderPartLine)oLineNode.Value).IsSameAlpha((BorderPartLine)oLineNode1.Value) && ((BorderPartLine)oLineNode.Value).IsSameAlpha((BorderPartLine)oLineNode2.Value) && ((BorderPartLine)oLineNode1.Value).IsSameAlpha((BorderPartLine)oLineNode2.Value))
                        {
                            if (((BorderPartLine)oLineNode.Value).CombineSameLinexx((BorderPartLine)oLineNode1.Value, (BorderPartLine)oLineNode2.Value) == true)
                            {
                                BorderLines.Remove(oLineNode1.Value);
                                BorderLines.Remove(oLineNode2.Value);
                            }
                        }
                    }
                    oLineNode = GetNextNode(oLineNode);
                }
                DebugStatic.oDebugWatch.StopWatch(currentStep, BorderLines.Count().ToString());
                DebugStatic.oDebugBitmap.MakeBitmap(currentStep, BorderLines.ToArray());
                _tracerParam.SetBorderLine(BorderLines.ToList(), currentStep);
            }

            currentStep = "Combine line with same alpha";
            DebugStatic.oDebugWatch.StartWatch(currentStep);
            oLineNode = BorderLines.First;
            turns = BorderLines.Count();
            for (int z = 0; z < 3; z++)
            {
                double dPixel = 1.0 + 0.5 * z;

                for (int i = 0; i < turns * 2; i++)
                {
                    oLineNode1 = GetNextNode(oLineNode);
                    oLineNode2 = GetNextNode(oLineNode1);

                    double dA1 = ((BorderPartLine)oLineNode1.Value).GetDiffAlphaAbs((BorderPartLine)oLineNode.Value);
                    double dA2 = ((BorderPartLine)oLineNode1.Value).GetDiffAlphaAbs((BorderPartLine)oLineNode2.Value);
                    if (dA1 < dA2 || (dA1 == dA2 && ((BorderPartLine)oLineNode.Value).m_nSize > ((BorderPartLine)oLineNode2.Value).m_nSize))
                    {
                        if (((BorderPartLine)oLineNode.Value).CombineSameAlpha((BorderPartLine)oLineNode1.Value, dPixel))
                        {
                            BorderLines.Remove(oLineNode1.Value);
                        }
                        else if (((BorderPartLine)oLineNode1.Value).CombineSameAlpha((BorderPartLine)oLineNode2.Value, dPixel))
                        {
                            BorderLines.Remove(oLineNode2.Value);
                        }
                        else
                        {
                            oLineNode = GetNextNode(oLineNode);
                        }
                    }
                    else
                    {
                        if (((BorderPartLine)oLineNode1.Value).CombineSameAlpha((BorderPartLine)oLineNode2.Value, dPixel))
                        {
                            BorderLines.Remove(oLineNode2.Value);
                        }
                        else if (((BorderPartLine)oLineNode.Value).CombineSameAlpha((BorderPartLine)oLineNode1.Value, dPixel))
                        {
                            BorderLines.Remove(oLineNode1.Value);
                        }
                        else
                        {
                            oLineNode = GetNextNode(oLineNode);
                        }
                    }
                }
            }
            DebugStatic.oDebugWatch.StopWatch(currentStep, BorderLines.Count().ToString());
            DebugStatic.oDebugBitmap.MakeBitmap(currentStep, BorderLines.ToArray());
            _tracerParam.SetBorderLine(BorderLines.ToList(), currentStep);

            currentStep = "Combine corner line with same alpha";
            DebugStatic.oDebugWatch.StartWatch(currentStep);
            oLineNode = BorderLines.First;
            turns = BorderLines.Count();
            for (int i = 0; i < turns; i++)
            {
                double dPixel = 4.0;
                oLineNode1 = GetNextNode(oLineNode);
                oLineNode2 = GetNextNode(oLineNode1);

                double dA3 = ((BorderPartLine)oLineNode.Value).GetDiffAlphaAbs((BorderPartLine)oLineNode2.Value);
                if (dA3 > 50 && dA3 < 130)
                {
                    double dA1 = ((BorderPartLine)oLineNode1.Value).GetDiffAlphaAbs((BorderPartLine)oLineNode.Value);
                    double dA2 = ((BorderPartLine)oLineNode1.Value).GetDiffAlphaAbs((BorderPartLine)oLineNode2.Value);
                    if (dA1 < dA2)
                    {
                        if (((BorderPartLine)oLineNode.Value).CombineSameAlpha((BorderPartLine)oLineNode1.Value, dPixel) == true)
                        {
                            BorderLines.Remove(oLineNode1.Value);
                        }
                    }
                    else
                    {
                        if (((BorderPartLine)oLineNode1.Value).CombineSameAlpha((BorderPartLine)oLineNode2.Value, dPixel) == true)
                        {
                            BorderLines.Remove(oLineNode2.Value);
                        }
                    }
                }
                oLineNode = GetNextNode(oLineNode);
            }
            DebugStatic.oDebugWatch.StopWatch(currentStep, BorderLines.Count().ToString());
            DebugStatic.oDebugBitmap.MakeBitmap(currentStep, BorderLines.ToArray());
            _tracerParam.SetBorderLine(BorderLines.ToList(), currentStep);


            currentStep = "Remove corner line (size=2-6)";
            DebugStatic.oDebugWatch.StartWatch(currentStep);
            oLineNode = BorderLines.First;
            turns = BorderLines.Count();
            for (int i = 0; i < turns; i++)
            {
                if (((BorderPartLine)oLineNode.Value).m_nSize < 7)
                {
                    oLineNode1 = GetNextNode(oLineNode);
                    oLineNodeBack1 = GetPreviousNode(oLineNode);
                    //double dA1 = ((BorderPartLine)oLineNode.Value).GetDiffAlphaAbs((BorderPartLine)oLineNode1.Value);
                    //double dA2 = ((BorderPartLine)oLineNode.Value).GetDiffAlphaAbs((BorderPartLine)oLineNodeBack1.Value);
                    double dA3 = ((BorderPartLine)oLineNode1.Value).GetDiffAlphaAbs((BorderPartLine)oLineNodeBack1.Value);
                    //if (dA1 > 20 && dA1 < 70 && dA2 > 20 && dA2 < 70 && dA3 > 60)
                    if (dA3 > 50 && dA3 < 130)
                    {
                        BorderLines.Remove(oLineNode.Value);
                        oLineNode = oLineNodeBack1;
                    }
                }
                oLineNode = GetNextNode(oLineNode);
            }
            DebugStatic.oDebugWatch.StopWatch(currentStep, BorderLines.Count().ToString());
            DebugStatic.oDebugBitmap.MakeBitmap(currentStep, BorderLines.ToArray());
            _tracerParam.SetBorderLine(BorderLines.ToList(), currentStep);

            // combine line to arc
            if (_tracerParam.ServerEventArgs.PointsPerCircle < 8) return;
            currentStep = "Convert line to arc";
            DebugStatic.oDebugWatch.StartWatch(currentStep);
            oLineNode = BorderLines.First;
            turns = BorderLines.Count;
            if (turns < 5) return;
            // find start
            for (int i = 0; i < turns; i++)
            {
                oLineNode1 = GetNextNode(oLineNode);
                if (((BorderPartLine)oLineNode.Value).GetDiffAlphaAbs((BorderPartLine)oLineNode1.Value) >= 45)
                {
                    i = turns * 3;
                }
                oLineNode = oLineNode1;
            }
            BorderPartArc oBorderPartArc = new BorderPartArc();
            oLineNodeStart = oLineNode;
            oLineNode1 = GetNextNode(oLineNode);
            List<BorderPartLine> listLine;
            do
            {
                int nMinCount = 0;
                double dAlpha = 0;
                if (((BorderPartLine)oLineNode.Value).GetDiffAlphaAbs((BorderPartLine)oLineNode1.Value) < 45)
                {
                    int nDir = ((BorderPartLine)oLineNode.Value).GetDiffAlphaAbs((BorderPartLine)oLineNode1.Value) > 0 ? 1 : -1;
                    listLine = new List<BorderPartLine>();
                    listLine.Add((BorderPartLine)oLineNode.Value);
                    listLine.Add((BorderPartLine)oLineNode1.Value);

                    double dAlphaPart;
                    while (oLineNode1 != oLineNodeStart)
                    {
                        oLineNode2 = GetNextNode(oLineNode1);
                        if (oLineNode2 == oLineNodeStart || oLineNode2.Value.GetType() != typeof(BorderPartLine))
                        {
                            break;
                        }
                        dAlphaPart = ((BorderPartLine)oLineNode1.Value).GetDiffAlphaAbs((BorderPartLine)oLineNode2.Value) * nDir;
                        if (dAlphaPart <= 0 || dAlphaPart >= 45)
                        {
                            break;
                        }
                        listLine.Add((BorderPartLine)oLineNode2.Value);
                        oLineNode1 = oLineNode2;

                        if (dAlpha < 40) nMinCount++;
                        dAlpha += dAlphaPart;
                    }
                    if (dAlpha >= 40)
                    {
                        if (nMinCount < 5) nMinCount = 5;

                        EllipseFit oEllipseFit;
                        bool bFound = false;
                        while (bFound == false && listLine.Count >= nMinCount)
                        {
                            List<Point> listCheckPoints = new List<Point>();
                            listCheckPoints.Add(listLine[0].m_oPointStart);
                            foreach (BorderPartLine oBPL in listLine)
                            {
                                listCheckPoints.Add(GetPointInTheMiddle(oBPL.m_oPointStart, oBPL.m_oPointEnd, 0.5));
                                listCheckPoints.Add(oBPL.m_oPointEnd);
                            }
                            oBorderPartArc = new BorderPartArc();
                            List<Point> listPoints = GetPointListMax(listLine[0].m_oPointStart, listLine[listLine.Count - 1].m_oPointEnd, 2);

                            //string strTemp = "EllipseFit2";
                            //DebugStatic.oDebugWatch.StartWatch(strTemp);
                            oEllipseFit = new EllipseFit();
                            oEllipseFit.Fit(listPoints);
                            //DebugStatic.oDebugWatch.StopWatch(strTemp);

                            foreach (Ellipse oE in oEllipseFit.m_listEllipse)
                            {
                                if (oBorderPartArc.Point2Ellipse(oE, listPoints, listCheckPoints) == true)
                                {
                                    bFound = true;
                                    break;
                                }
                            }
                            if (bFound == false) listLine.RemoveAt(listLine.Count - 1);
                        }
                        if (listLine.Count >= nMinCount)
                        {
                            if (listLine.Count == BorderLines.Count())
                            {
                                BorderLines.Clear();
                                BorderLines.AddFirst(oBorderPartArc);
                                oLineNode = BorderLines.First;
                            }
                            else
                            {
                                oLineNode = GetPreviousNode(oLineNode);
                                BorderLines.AddAfter(oLineNode, oBorderPartArc);
                                foreach (BorderPartLine oBPL in listLine)
                                {
                                    BorderLines.Remove(oBPL);
                                }
                                oLineNode = GetNextNode(oLineNode);
                                oBorderPartArc.m_oLineStart = listLine[0];
                                oBorderPartArc.m_oLineEnd = listLine[listLine.Count - 1];
                            }
                        }
                    }
                }
                oLineNode = GetNextNode(oLineNode);
                oLineNode1 = GetNextNode(oLineNode);
            }
            while (oLineNode != oLineNodeStart && oLineNode.Value.GetType() == typeof(BorderPartLine) && oLineNode1 != oLineNodeStart && oLineNode1.Value.GetType() == typeof(BorderPartLine));

            DebugStatic.oDebugWatch.StopWatch(currentStep, BorderLines.Count().ToString());
            DebugStatic.oDebugBitmap.MakeBitmap(currentStep, BorderLines.ToArray());
            _tracerParam.SetBorderLine(BorderLines.ToList(), currentStep);
        }

        public List<BorderPart> GetPoints()
        {
            List<BorderPart> borderParts = new List<BorderPart>();
            StraightLine line;
            StraightLine otherLine;
            CarthesianPoint oPointD;
            BorderPart borderPart;
            BorderPartLine borderPartLine;
            BorderPartArc borderPartArc;
            List<CarthesianPoint> carthesianPoints;
            string stepName = "Convert to geographic points";

            if (BorderLines.Count == 1)
            {
                // only one ellipse
                borderParts = new List<BorderPart>();
                carthesianPoints = ((BorderPartArc)BorderLines.First.Value).GetBorderPoints(_tracerParam.ServerEventArgs.PointsPerCircle);
                foreach (CarthesianPoint carthesianPoint in carthesianPoints)
                {
                    if (double.IsNaN(carthesianPoint.X) || double.IsNaN(carthesianPoint.Y))
                    {
                        _tracerParam.Error = "Point's dimensions are invalid: " + carthesianPoint.ToString();
                        _tracerParam.SetBorderLine(borderParts, stepName);
                        return null;
                    }
                    borderParts.Add(new BorderPartPointGeo(_tracerParam.GeoMap.ConvertPointToGeo(carthesianPoint)));
                }
                _tracerParam.SetBorderLine(borderParts, stepName);
                return borderParts;
            }

            // get first line
            borderPart = BorderLines.Last();
            if (borderPart.GetType() == typeof(BorderPartLine))
            {
                borderPartLine = (BorderPartLine)borderPart;
            }
            else
            {
                borderPartLine = ((BorderPartArc)borderPart).m_oLineEnd;
            }
            line = CalculateStraightLine(borderPartLine);

            // get all points
            foreach (BorderPart oBP in BorderLines)
            {
                if (oBP.GetType() == typeof(BorderPartLine))
                {
                    borderPartLine = (BorderPartLine)oBP;
                    otherLine = CalculateStraightLine(borderPartLine);

                    oPointD = GetIntersectionPoint(line, otherLine);
                    if (double.IsNaN(oPointD.X) || double.IsNaN(oPointD.Y))
                    {
                        _tracerParam.Error = "No intersection point: " + line.ToString() + " " + otherLine.ToString();
                        _tracerParam.SetBorderLine(borderParts, stepName);
                        return null;
                    }
                    borderParts.Add(new BorderPartPointGeo(_tracerParam.GeoMap.ConvertPointToGeo(oPointD)));
                    line = otherLine;
                }
                else
                {
                    borderPartArc = (BorderPartArc)oBP;
                    borderPartLine = borderPartArc.m_oLineStart;
                    otherLine = CalculateStraightLine(borderPartLine);

                    // get start point
                    oPointD = GetIntersectionPoint(line, otherLine);
                    if (double.IsNaN(oPointD.X) || double.IsNaN(oPointD.Y))
                    {
                        _tracerParam.Error = "No intersection point: " + line.ToString() + " " + otherLine.ToString();
                        _tracerParam.SetBorderLine(borderParts, stepName);
                        return null;
                    }
                    borderParts.Add(new BorderPartPointGeo(_tracerParam.GeoMap.ConvertPointToGeo(oPointD)));

                    // get end point
                    borderPartLine = borderPartArc.m_oLineEnd;
                    line = CalculateStraightLine(borderPartLine);

                    borderPart = GetNextNode(BorderLines.Find(oBP)).Value;
                    if (borderPart.GetType() == typeof(BorderPartLine))
                    {
                        borderPartLine = (BorderPartLine)borderPart;
                    }
                    else
                    {
                        borderPartLine = ((BorderPartArc)borderPart).m_oLineStart;
                    }
                    otherLine = CalculateStraightLine(borderPartLine);
                    CarthesianPoint oPointD2 = GetIntersectionPoint(line, otherLine);
                    if (double.IsNaN(oPointD2.X) || double.IsNaN(oPointD2.Y))
                    {
                        _tracerParam.Error = "No intersection point: " + line.ToString() + " " + otherLine.ToString();
                        _tracerParam.SetBorderLine(borderParts, stepName);
                        return null;
                    }

                    // get ellipse points
                    carthesianPoints = borderPartArc.GetBorderPoints(oPointD, oPointD2, _tracerParam.ServerEventArgs.PointsPerCircle);
                    foreach (CarthesianPoint oPD in carthesianPoints)
                    {
                        if (double.IsNaN(oPD.X) || double.IsNaN(oPD.Y))
                        {
                            _tracerParam.Error = "Point is not a nummber: " + oPD.ToString();
                            _tracerParam.SetBorderLine(borderParts, stepName);
                            return null;
                        }
                        borderParts.Add(new BorderPartPointGeo(_tracerParam.GeoMap.ConvertPointToGeo(oPD)));
                    }
                }
            }
            _tracerParam.SetBorderLine(borderParts, stepName);
            if (borderParts.Count <= 2)
            {
                // no area
                _tracerParam.Error = "No area found.";
                return null;
            }
            return borderParts;
        }

        private CarthesianPoint GetIntersectionPoint(StraightLine line, StraightLine other)
        {
            CarthesianPoint intersectionPoint = line.GetIntersectionPoint(other);

            if (intersectionPoint.X < 0 || intersectionPoint.X > _tracerParam.GeoMap.XYMax || intersectionPoint.Y < 0 || intersectionPoint.Y > _tracerParam.GeoMap.XYMax)
            {
                return new CarthesianPoint(double.NaN, double.NaN);
            }
            if (double.IsNaN(intersectionPoint.X))
            {
                return new CarthesianPoint(double.NaN, double.NaN);
            }
            return intersectionPoint;
        }

        private Point GetPointInTheMiddle(Point startPoint, Point endPoint, double dPos)
        {
            int indexOfStartPoint = BorderPoints.IndexOf(startPoint);
            int indexOfEndpoint = BorderPoints.IndexOf(endPoint);
            int count;
            int nPos;

            if (indexOfStartPoint > indexOfEndpoint)
            {
                count = BorderPoints.Count - indexOfStartPoint + indexOfEndpoint;
            }
            else
            {
                count = indexOfEndpoint - indexOfStartPoint;
            }
            count = (int)(dPos * count);
            nPos = indexOfStartPoint + count;
            if (nPos >= BorderPoints.Count)
            {
                nPos = nPos - BorderPoints.Count();
            }
            return BorderPoints.ElementAt(nPos);
        }

        private List<Point> GetPointListMax(Point startPoint, Point endPoint, int nRemove)
        {
            List<Point> points;

            if (startPoint != endPoint)
            {
                points = GetPoints(startPoint, endPoint);
                if (points.Count > nRemove * 2)
                {
                    points.RemoveRange(0, nRemove);
                    points.RemoveRange(points.Count - nRemove, nRemove);
                }
            }
            else
            {
                points = new List<Point>(BorderPoints.ToArray());
            }

            if (points.Count > 700)
            {
                int amountOfSteps = points.Count / 500;
                List<Point> result = new List<Point>();
                for (int i = 0; i < points.Count - 1; i += amountOfSteps)
                {
                    result.Add(points[i]);
                }
                return result;
            }
            return points;
        }

        private List<Point> GetPoints(Point startPoint, Point endPoint)
        {
            List<Point> listPoint = new List<Point>();

            int startPointIndex = BorderPoints.IndexOf(startPoint);
            int endPointIndex = BorderPoints.IndexOf(endPoint);
            for (int i = 0; i < BorderPoints.Count; i++)
            {
                if (BorderPoints[i] == startPoint)
                {
                    startPointIndex = i;
                }
                if (BorderPoints[i] == endPoint)
                {
                    endPointIndex = i;
                }
            }

            if (startPointIndex > endPointIndex)
            {
                for (int i = startPointIndex; i < BorderPoints.Count(); i++)
                {
                    listPoint.Add(BorderPoints[i]);
                }
                for (int i = 0; i <= endPointIndex; i++)
                {
                    listPoint.Add(BorderPoints[i]);
                }
            }
            else
            {
                for (int i = startPointIndex; i <= endPointIndex; i++)
                {
                    listPoint.Add(BorderPoints[i]);
                }
            }
            return listPoint;
        }

        private StraightLine CalculateStraightLine(BorderPartLine oLine)
        {
            bool bRevert;

            StraightLine oStraightLineFit;
            StraightLine oStraightLine;

            List<Point> listPoints = GetPoints(oLine.m_oPointStart, oLine.m_oPointEnd);
            LineFit oLineFit = new LineFit();
            oLineFit.Fit(listPoints);
            double slope = oLineFit.m_dm;
            double yIntercept = oLineFit.m_dc;
            bRevert = oLineFit.m_bRevert;

            oStraightLineFit = new StraightLine(slope, yIntercept, bRevert);
            oStraightLine = new StraightLine((CarthesianPoint)oLine.m_oPointStart, (CarthesianPoint)oLine.m_oPointEnd);

            if (oStraightLineFit.IsLine)
            {
                // check alpha
                double dDif = StraightLine.CheckAlpha(oStraightLine.Alpha - oStraightLineFit.Alpha);
                if ((dDif < 20 && dDif > -20) || dDif > 180 - 20 || dDif < -180 + 20)
                {
                    oStraightLine = oStraightLineFit;
                }
            }
            return oStraightLine;
        }

        private LinkedListNode<BorderPart> GetNextNode(LinkedListNode<BorderPart> oNode)
        {
            LinkedListNode<BorderPart> oNext = oNode.Next;
            if (oNext == null)
            {
                oNext = BorderLines.First;
            }
            return oNext;
        }

        private LinkedListNode<BorderPart> GetPreviousNode(LinkedListNode<BorderPart> oNode)
        {
            LinkedListNode<BorderPart> oPrevious = oNode.Previous;
            if (oPrevious == null)
            {
                oPrevious = BorderLines.Last;
            }
            return oPrevious;
        }

    }

}
