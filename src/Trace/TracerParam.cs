﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Diagnostics;
using Tracer2Server.Tiles;
using Tracer2Server.Projection;
using Tracer2Server.WebServer;

namespace Tracer2Server.Trace
{
    internal delegate void ServerEventHandler(object sender, EventType e);

    public enum EventType { Start, AddTile, AddBorderPoints, AddBorderStep, Stop };

    internal class TracerParam : EventArgs
    {
        public static event ServerEventHandler ServerEventHandler;

        public ServerEventArgs ServerEventArgs;

        public Tiler Tiler;
        public GeoMap GeoMap;
        internal TileCache FileCache;

        public GeographicPoint InnerGeographicPoint;
        public Point InnerPointXY;
        public Color InnerColor;
        public Rectangle ShapeAreaXY;

        public TimeSpan TimeSpan;
        private readonly Stopwatch _stopWatch;

        public List<TileBitmap> tileBitmaps = new List<TileBitmap>();
        private readonly object _lock = new object();

        public string HeadLine = "";
        public string Result = "";
        public string Output = "";
        public string Error = "";
        public string StackTrace = "";

        internal Point StartPointXY;
        internal List<Point> BorderPoints;
        internal List<BorderPart[]> BorderSteps = new List<BorderPart[]>();
        internal List<string> BorderStepsNames = new List<string>();

        public TracerParam(ServerEventArgs args)
        {
            ServerEventArgs = args;

            Tiler = new Tiler(this);
            GeoMap = new GeoMap(this);
            FileCache = new TileCache(this);

            InnerGeographicPoint = args.StartPoint;
            InnerPointXY = GeoMap.ConvertGeoToPoint(InnerGeographicPoint);
            InnerColor = Color.Black;

            _stopWatch = new Stopwatch();
            _stopWatch.Start();

            HeadLine = "Trace  " + InnerGeographicPoint.ToString();
            FireEvent(EventType.Start);
        }

        public void AddNewTile(TileBitmap tileBitmap)
        {
            if (tileBitmap != null)
            {
                lock (_lock)
                {
                    string timeString = string.Format("{0:00}.{1:000}", tileBitmap.TimeSpan.Seconds, tileBitmap.TimeSpan.Milliseconds);
                    if (tileBitmap.ShouldDownloadViaWms)
                    {
                        Output += "Download tile  Rect=" + tileBitmap.Tile.ToString() + "  Time=" + timeString + "s\r\n";
                    }
                    else
                    {
                        Output += "Load tile  Rect=" + tileBitmap.Tile.ToString() + "  Time=" + timeString + "s\r\n";
                    }
                    tileBitmaps.Add(tileBitmap);

                    FireEvent(EventType.AddTile);
                }
            }
        }

        public void SetStartPoint(Point startPointXY)
        {
            if (startPointXY != null)
            {
                lock (_lock)
                {
                    StartPointXY = startPointXY;
                }
            }
        }

        public void SetBorderPoints(List<Point> borderPoints)
        {
            if (borderPoints != null)
            {
                lock (_lock)
                {
                    BorderPoints = borderPoints;

                    Output += "Get border points -> " + borderPoints.Count + " points\r\n";

                    FireEvent(EventType.AddBorderPoints);
                }
            }
        }

        public void SetBorderLine(List<BorderPart> borderParts, string name)
        {
            int lineCount = 0;
            int arcCount = 0;
            int geographicPointCount = 0;
            string tempString = "";

            if (borderParts != null)
            {
                lock (_lock)
                {
                    List<BorderPart> borderPartsCopy = new List<BorderPart>();
                    foreach (BorderPart borderPart in borderParts)
                    {
                        if (borderPart.GetType() == typeof(BorderPartLine))
                        {
                            lineCount++;
                        }
                        else if (borderPart.GetType() == typeof(BorderPartArc))
                        {
                            arcCount++;
                        }
                        else if (borderPart.GetType() == typeof(BorderPartPointGeo))
                        {
                            geographicPointCount++;
                        }
                        else
                        {
                            continue;
                        }
                        borderPartsCopy.Add(borderPart.ShallowCopy());
                    }
                    BorderSteps.Add(borderPartsCopy.ToArray());

                    if (lineCount > 0)
                    {
                        tempString += lineCount.ToString() + " lines ";
                    }
                    if (arcCount > 0)
                    {
                        tempString += arcCount.ToString() + " arc ";
                    }
                    if (geographicPointCount > 0)
                    {
                        tempString += geographicPointCount.ToString() + " points ";
                    }
                    tempString = name + " -> " + tempString.Trim();
                    BorderStepsNames.Add(tempString);

                    Output += tempString + "\r\n";

                    FireEvent(EventType.AddBorderStep);
                }
            }
        }

        public void SetError(string error)
        {
            lock (_lock)
            {
                Output += error + "\r\n";
                Error = error;
            }
        }

        public void Stop()
        {
            lock (_lock)
            {
                _stopWatch.Stop();
                TimeSpan = _stopWatch.Elapsed;

                if (Error.Equals(""))
                {
                    Result = ServerEventArgs.Order + " " + InnerGeographicPoint.ToString() + " finishes -> " + BorderSteps[BorderSteps.Count - 1].Length + " points";
                }
                else
                {
                    Result = ServerEventArgs.Order + " " + InnerGeographicPoint.ToString() + " error -> " + Error;
                }
                string time = string.Format("{0:00}.{1:000}", TimeSpan.Seconds, TimeSpan.Milliseconds);
                Output += "Request finishes  Time=" + time + "s\r\n";

                FileCache.ClearAllData();
                GC.Collect();

                FireEvent(EventType.Stop);
            }
        }

        private void FireEvent(EventType oEventType)
        {
            ServerEventHandler?.Invoke(this, oEventType);
        }
    }
}
