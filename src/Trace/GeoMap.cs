﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System;
using System.Drawing;
using Tracer2Server.Tiles;
using Tracer2Server.Projection;

namespace Tracer2Server.Trace
{
    internal class GeoMap
    {
        private readonly TracerParam TracerParam;

        private readonly int TileArraySize;
        private readonly int TileCenter;

        private readonly GeographicPoint InnerPoint;

        private readonly int Resolution;
        private readonly int ResolutionMask;
        private readonly int ResolutionShift;

        private Point FirstTileNr;
        private readonly double TileSize;

        private Object[,] m_aoData;

        public int XYMax;
        private readonly GeographicRectangle RectangleGeoAll;

        public GeoMap(TracerParam tracerParam)
        {
            TracerParam = tracerParam;

            Tiler tiler = TracerParam.Tiler;

            InnerPoint = tracerParam.ServerEventArgs.StartPoint;

            TileSize = tiler.TileSize;
            Resolution = tiler.Resultion;
            ResolutionMask = Resolution - 1;
            ResolutionShift = tiler.ResolutionShift;

            XYMax = 65536 - 1;
            TileArraySize = (XYMax + 1) / Resolution;
            TileCenter = TileArraySize / 2;

            m_aoData = new Object[TileArraySize, TileArraySize];
            FirstTileNr = tiler.GetTileNrByPointGeo(InnerPoint);
            FirstTileNr.X -= TileCenter;
            FirstTileNr.Y -= TileCenter;

            RectangleGeoAll = new GeographicRectangle(
                (FirstTileNr.X) * TileSize,
                (FirstTileNr.Y) * TileSize,
                (FirstTileNr.X + TileArraySize) * TileSize,
                (FirstTileNr.Y + TileArraySize) * TileSize
                );
        }

        public byte GetPix(int inputX, int inputY)
        {
            if (inputX < 0 || inputX > XYMax || inputY < 0 || inputY > XYMax)
            {
                throw new Exception("Area to big");
            }

            int x = inputX & ResolutionMask;
            int y = ResolutionMask - (inputY & ResolutionMask);
            int nTX = inputX >> ResolutionShift;
            int nTY = inputY >> ResolutionShift;

            Object oData = m_aoData[nTX, nTY];
            if (oData == null)
            {
                GetTile(nTX, nTY);
                oData = m_aoData[nTX, nTY];
            }
            return ((byte[][])oData)[y][x];
        }

        private void GetTile(int tileX, int tileY)
        {
            Point pointXY = new Point(
                tileX * Resolution,
                tileY * Resolution
                );

            GeographicRectangle rectGeo = new GeographicRectangle(
                (FirstTileNr.X + tileX) * TileSize,
                (FirstTileNr.Y + tileY + 1) * TileSize,
                (FirstTileNr.X + tileX + 1) * TileSize,
                (FirstTileNr.Y + tileY) * TileSize
                );

            Tile tile = new Tile(rectGeo, pointXY, Resolution);

            m_aoData[tileX, tileY] = (Object)TracerParam.FileCache.GetTile(tile);
        }

        public GeographicPoint ConvertPointToGeo(CarthesianPoint carthesianPoint)
        {
            GeographicPoint geographicPoint = new GeographicPoint();

            double longitude = ((carthesianPoint.X + 1) / (Resolution * TileArraySize)) * (RectangleGeoAll.Right - RectangleGeoAll.Left) + RectangleGeoAll.Left;
            double latitude = (Resolution * TileArraySize - carthesianPoint.Y - 0.5) / (Resolution * TileArraySize) * (RectangleGeoAll.Top - RectangleGeoAll.Bottom) + RectangleGeoAll.Bottom;

            if (double.IsNaN(geographicPoint.Longitude) || double.IsNaN(geographicPoint.Latitude))
            {
                return new GeographicPoint(latitude, longitude);
            }
            return new GeographicPoint(latitude, longitude);
        }

        public Point ConvertGeoToPoint(GeographicPoint point)
        {
            return new Point(
                (int)Math.Round((point.Longitude - RectangleGeoAll.Left) / (RectangleGeoAll.Right - RectangleGeoAll.Left) * Resolution * TileArraySize),
                (Resolution * TileArraySize) - (int)Math.Round((point.Latitude - RectangleGeoAll.Bottom) / (RectangleGeoAll.Top - RectangleGeoAll.Bottom) * Resolution * TileArraySize)
                );
        }

    }

}
