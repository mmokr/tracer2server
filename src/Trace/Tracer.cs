﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System;
using System.Collections.Generic;
using Tracer2Server.Debug;
using Tracer2Server.Projection;

namespace Tracer2Server.Trace
{
    class Tracer
    {
        public GeographicPoint[] Trace(TracerParam tracerParam)
        {
            List<BorderPart> borderParts;
            ShapeFinder shapeFinder;
            GeographicPoint[] geographicPoints = null;

            if (tracerParam == null)
            {
                return null;
            }

            shapeFinder = new ShapeFinder(tracerParam);

            try
            {
                DebugStatic.oDebugWatch.StartWatch("GetStartPoint");
                shapeFinder.GetStartPoint();
                DebugStatic.oDebugWatch.StopWatch("GetStartPoint");

                DebugStatic.oDebugWatch.StartWatch("GetBorderPoints");
                shapeFinder.GetBorderPoints();
                DebugStatic.oDebugWatch.StopWatch("GetBorderPoints");

                DebugStatic.oDebugWatch.StartWatch("GetBorderLine");
                shapeFinder.GetBorderLine();
                DebugStatic.oDebugWatch.StopWatch("GetBorderLine");

                DebugStatic.oDebugWatch.StartWatch("GetPoints");
                borderParts = shapeFinder.GetPoints();
                DebugStatic.oDebugWatch.StopWatch("GetPoints");

                tracerParam.Stop();

                if (borderParts != null)
                {
                    geographicPoints = new GeographicPoint[borderParts.Count];
                    for (int i = 0; i < borderParts.Count; i++)
                    {
                        geographicPoints[i] = ((BorderPartPointGeo)borderParts[i]).GeographicPoint;
                    }
                }
            }
            catch (Exception e)
            {
                tracerParam.Error = "Exception: " + e.Message;
                tracerParam.StackTrace = e.StackTrace;
                tracerParam.Stop();
                return null;
            }
            return geographicPoints;
        }
    }
}
