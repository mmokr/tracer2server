﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System.Globalization;

namespace Tracer2Server.Projection
{
    public struct GeographicRectangle
    {
        private readonly GeographicPoint TopLeftPoint;
        private readonly GeographicPoint BottomRightPoint;

        public double Top
        {
            get { return TopLeftPoint.Latitude; }
        }

        public double Left
        {
            get { return TopLeftPoint.Longitude; }
        }

        public double Bottom
        {
            get { return BottomRightPoint.Latitude; }
        }

        public double Right
        {
            get { return BottomRightPoint.Longitude; }
        }

        public GeographicRectangle(GeographicPoint topLeft, GeographicPoint bottomRight)
        {
            TopLeftPoint = topLeft;
            BottomRightPoint = bottomRight;
        }

        public GeographicRectangle(double left, double top, double right, double bottom)
        {
            TopLeftPoint = new GeographicPoint(top, left);
            BottomRightPoint = new GeographicPoint(bottom, right);
        }

        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture, "{0:F4} {1:F4} {2:F4} {3:F4}", Left, Top, Right, Bottom);
        }

    }

}
