﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System.Drawing;
using System.Globalization;

namespace Tracer2Server.Projection
{
    public struct CarthesianPoint
    {
        public CarthesianPoint(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X { get; }

        public double Y { get; }

        public static CarthesianPoint operator +(CarthesianPoint first, CarthesianPoint other)
        {
            return new CarthesianPoint(first.X + other.X, first.Y + other.Y);
        }

        public static CarthesianPoint operator +(CarthesianPoint first, Point other)
        {
            return new CarthesianPoint(first.X + other.X, first.Y + other.Y);
        }

        public static explicit operator Point(CarthesianPoint point)
        {
            return new Point((int)point.X, (int)point.Y);
        }

        public static explicit operator CarthesianPoint(Point point)
        {
            return new CarthesianPoint(point.X, point.Y);
        }

        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture, "(x={0}, y={1})", X, Y);
        }

    }

}
