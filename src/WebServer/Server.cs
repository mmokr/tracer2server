﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Net;
using Tracer2Server.Trace;
using System.Globalization;
using Tracer2Server.Debug;
using Tracer2Server.Projection;
using System.Reflection;

namespace Tracer2Server.WebServer
{
    public class Server
    {
        private TcpListener _tcpListener;
        private bool started = false;

        public Server(int port)
        {
            Port = port;
        }

        public int Port { get; }

        public void Start()
        {
            IPAddress ipAddress;

            if (started)
            {
                return;
            }

            started = true;

            IPHostEntry host = Dns.GetHostEntry("localhost");
            if (OperationSystem.isWindows)
            {
                if (host.AddressList.Length > 1)
                {
                    ipAddress = host.AddressList[1];
                }
                else
                {
                    ipAddress = host.AddressList[0];
                }
            }
            else
            {
                ipAddress = host.AddressList[0];
            }
            _tcpListener = new TcpListener(ipAddress, Port);
            _tcpListener.Start();
            Thread th = new Thread(new ThreadStart(StartListen));
            th.IsBackground = true;
            th.Start();
        }

        private void SendHeader(string strHttpVersion, int nBytesLength, string strStatusCode, string strMime, ref Socket r_oSocket)
        {
            StringBuilder buffer = new StringBuilder();
            buffer = buffer.Append(strHttpVersion + strStatusCode + "\r\n");
            buffer = buffer.Append("Server: OSM.WebServer\r\n");
            buffer = buffer.Append(string.Format("Content-Type: {0}\r\n", strMime));
            buffer = buffer.Append("Accept-Ranges: bytes\r\n");
            buffer = buffer.Append("Content-Length: " + nBytesLength + "\r\n\r\n");
            byte[] sendData = Encoding.ASCII.GetBytes(buffer.ToString());
            SendToBrowser(sendData, ref r_oSocket);
        }

        private void SendToBrowser(byte[] sendData, ref Socket socket)
        {
            try
            {
                if (socket.Connected)
                {
                    socket.Send(sendData, sendData.Length, 0);
                }
            }
            catch
            {
            }
        }

        private void StartListen()
        {
            ServerEventArgs serverEventArgs;
            TracerParam tracerParam;

            while (true)
            {
                Socket socket = _tcpListener.AcceptSocket();
                if (socket.Connected)
                {
                    byte[] receive = new byte[1024];
                    int _ = socket.Receive(receive, receive.Length, 0);
                    string buffer = Encoding.ASCII.GetString(receive);
                    if (buffer.Substring(0, 3) != "GET")
                    {
                        socket.Close();
                        return;
                    }

                    int startPosition = buffer.LastIndexOf("HTTP");
                    string httpVersion = buffer.Substring(startPosition, 8);
                    string request = buffer.Substring(0, startPosition - 1);
                    request.Replace("\\", "/");

                    serverEventArgs = new ServerEventArgs();
                    if (serverEventArgs.SetArgs(request) == true)
                    {
                        tracerParam = new TracerParam(serverEventArgs);
                        GetContent(tracerParam);
                    }
                    else
                    {
                        tracerParam = new TracerParam(serverEventArgs);
                        tracerParam.ServerEventArgs.Code = 400;
                        tracerParam.SetError("The request cannot be fulfilled due to bad syntax.");
                        tracerParam.Stop();

                        tracerParam.ServerEventArgs.Response = "&traceError=" + tracerParam.Error;
                    }
                    byte[] bytes = Encoding.UTF8.GetBytes(tracerParam.ServerEventArgs.Response);
                    SendHeader(httpVersion, bytes.Length, string.Format(" {0} OK", serverEventArgs.Code), serverEventArgs.Mime, ref socket);
                    SendToBrowser(bytes, ref socket);
                    socket.Close();
                }
            }
        }

        private void GetContent(TracerParam tracerParam)
        {
            Console.WriteLine("- {0}/{1}/{2}", tracerParam.ServerEventArgs.Order, tracerParam.ServerEventArgs.Latitude, tracerParam.ServerEventArgs.Longitude);

            switch (tracerParam.ServerEventArgs.Order)
            {
                case "GetTrace":
                    try
                    {
                        tracerParam.ServerEventArgs.Response = GetTrace(tracerParam);
                    }
                    catch (Exception e)
                    {
                        tracerParam.Error = "Exception: " + e.Message;
                        tracerParam.StackTrace = e.StackTrace.ToString();
                        tracerParam.ServerEventArgs.Response = "&traceError=" + tracerParam.Error;
                        tracerParam.Stop();
                    }
                    break;
                case "GetVersion":
                    Version Version = Assembly.GetCallingAssembly().GetName().Version;
                    tracerParam.ServerEventArgs.Response = Version.Major.ToString() + ":" + Version.Minor.ToString() + ":" + Version.Build.ToString() + ":" + Version.Revision.ToString();
                    break;
                default:
                    tracerParam.ServerEventArgs.Code = 404;
                    break;
            }
        }

        private string GetTrace(TracerParam tracerParam)
        {
            NumberFormatInfo defaultNumberFormat = NumberFormatInfo.InvariantInfo;
            Tracer tracer = new Tracer();

            DebugStatic.oDebugWatch.StartWatch("TraceSimple");
            GeographicPoint[] geographicPoints = tracer.Trace(tracerParam);
            DebugStatic.oDebugWatch.StopWatch("TraceSimple");

            if (geographicPoints == null)
            {
                return "&traceError=" + tracerParam.Error;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("tracePoints=");
            foreach (GeographicPoint point in geographicPoints)
            {
                sb.Append(string.Format(defaultNumberFormat, "({0:F8}:{1:F8})", point.Latitude, point.Longitude));
            }
            if (tracerParam.ServerEventArgs.Mode == "match color")
            {
                sb.Append("&traceColorARGB=");
                sb.Append("(");
                sb.Append(tracerParam.InnerColor.A.ToString());
                sb.Append(":");
                sb.Append(tracerParam.InnerColor.R.ToString());
                sb.Append(":");
                sb.Append(tracerParam.InnerColor.G.ToString());
                sb.Append(":");
                sb.Append(tracerParam.InnerColor.B.ToString());
                sb.Append(")");
            }
            return sb.ToString();
        }
    }
}
