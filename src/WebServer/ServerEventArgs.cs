﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Drawing;
using Tracer2Server.Projection;

namespace Tracer2Server.WebServer
{
    public class ServerEventArgs
    {
        private string m_strRequest;
        
        private readonly NumberFormatInfo DefaultNumberFormatInfo = NumberFormatInfo.InvariantInfo;

        public string Order { get; private set; }

        public string Response { get; set; } = "";

        public string Mime { get; set; } = "text/plain";

        public int Code { get; set; } = 200;

        public double Latitude { get; private set; }

        public double Longitude { get; private set; }

        public string Name { get; private set; }

        public string Url { get; private set; }

        public double TileSize { get; private set; }

        public int Resolution { get; private set; }

        public int SkipBottom { get; private set; }

        public string Mode { get; private set; }

        public int Threshold { get; private set; }

        public int PointsPerCircle { get; private set; }

        public GeographicPoint StartPoint
        {
            get { return new GeographicPoint(Latitude, Longitude); }
        }

        public bool IsModeBoundary
        {
            get { return Mode.Equals("boundary"); }
        }

        private string GetString(string strParamName, string strDefault = "")
        {
            int startPos;
            int endPos;

            startPos = m_strRequest.IndexOf(strParamName);
            if (startPos < 0)
            {
                return strDefault;
            }
            endPos = m_strRequest.IndexOf("&trace", startPos + 1);

            if (endPos < 0)
            {
                endPos = m_strRequest.Length;
            }
            return m_strRequest.Substring(startPos + strParamName.Length, endPos - startPos - strParamName.Length).Trim();
        }

        private double GetDouble(string parameterName, string parmaterDefault)
        {
            return double.Parse(GetString(parameterName, parmaterDefault), DefaultNumberFormatInfo);
        }

        private int GetInt(string parameterName, string parmaterDefault)
        {
            return int.Parse(GetString(parameterName, parmaterDefault), DefaultNumberFormatInfo);
        }

        public bool SetArgs(string buffer)
        {
            try
            {
                m_strRequest = buffer;

                Order = GetString("traceOrder=");
                Latitude = GetDouble("&traceLat=", "0");
                Longitude = GetDouble("&traceLon=", "0");
                Name = GetString("&traceName=");
                Url = GetString("&traceUrl=");
                TileSize = GetDouble("&traceTileSize=", "0.0004");
                Resolution = GetInt("&traceResolution=", "2048");
                SkipBottom = GetInt("&traceSkipBottom=", "0");
                Mode = GetString("&traceMode=", "boundary");
                Threshold = GetInt("&traceThreshold=", "127");
                PointsPerCircle = GetInt("&tracePointsPerCircle=", "16");

                if (SkipBottom > Resolution / 10)
                {
                    SkipBottom = Resolution / 10;
                }

                if (Math.Abs(Latitude) > 90 || Math.Abs(Longitude) > 180)
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
