﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System.Drawing.Imaging;
using System.Drawing;
using System.Threading;
using Tracer2Server.Trace;

namespace Tracer2Server.Tiles
{
    internal class TilePreparer
    {
        const byte AREA = 0;
        const byte BORDER = 1;

        public byte[][] Prepare(Bitmap bitmap, TracerParam tracerParam)
        {
            int height = bitmap.Height;
            int width = bitmap.Width;
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
            int bitmapStride = bitmapData.Stride;
            int threshold = tracerParam.ServerEventArgs.Threshold;
            byte[][] result = new byte[height][];
            
            for (int x = 0; x < result.Length; x++)
            {
                result[x] = new byte[width];
            }

            unsafe
            {
                byte* pucBitmapData = (byte*)(void*)bitmapData.Scan0;
                int nYpos = 0;
                byte[] aucRow;

                switch (tracerParam.ServerEventArgs.Mode)
                {
                    case "boundary":
                        for (int y = 0; y < height; y++)
                        {
                            aucRow = result[y];
                            for (int x = 0; x < width; x++)
                            {
                                if (pucBitmapData[x * 3 + 2 + nYpos] < threshold && pucBitmapData[x * 3 + 1 + nYpos] < threshold && pucBitmapData[x * 3 + 0 + nYpos] < threshold)
                                    aucRow[x] = BORDER;
                                else
                                    aucRow[x] = AREA;
                            }
                            nYpos += bitmapStride;
                        }
                        break;
                    case "match color":
                        int nReTry = 1600; // 16 seconds
                        while ((nReTry > 0) && (tracerParam.InnerColor == Color.Black))
                        {
                            Thread.Sleep(10);
                            nReTry--;
                        }
                        if (nReTry > 0)
                        {
                            int nR = tracerParam.InnerColor.R;
                            int nG = tracerParam.InnerColor.G;
                            int nB = tracerParam.InnerColor.B;
                            int nRx;
                            int nGx;
                            int nBx;

                            for (int y = 0; y < height; y++)
                            {
                                aucRow = result[y];
                                for (int x = 0; x < width; x++)
                                {
                                    nBx = pucBitmapData[x * 3 + 0 + nYpos] - nB;
                                    nGx = pucBitmapData[x * 3 + 1 + nYpos] - nG;
                                    nRx = pucBitmapData[x * 3 + 2 + nYpos] - nR;

                                    nBx = nBx < 0 ? -nBx : nBx;
                                    nGx = nGx < 0 ? -nGx : nGx;
                                    nRx = nRx < 0 ? -nRx : nRx;

                                    if (nBx + nGx + nRx > threshold)
                                        aucRow[x] = BORDER;
                                    else
                                        aucRow[x] = AREA;
                                }
                                nYpos = nYpos + bitmapStride;
                            }
                        }
                        else
                        {
                            result = null;
                        }

                        break;
                    default:
                        result = null;
                        break;
                }
            }
            bitmap.UnlockBits(bitmapData);

            return result;
        }
    }
}
