﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System;
using System.Threading;
using System.IO;
using System.Drawing;
using System.Diagnostics;
using Tracer2Server.Debug;
using Tracer2Server.Trace;

namespace Tracer2Server.Tiles
{
    internal class TileBitmap
    {
        private readonly TracerParam _tracerParam;

        public Tile Tile;
        public string FileName;
        private byte[][] bitmapData;
        private bool ShouldClearData;

        private Thread Thread;
        private readonly object Lock = new object();

        public TimeSpan TimeSpan;
        public bool ShouldDownloadViaWms;

        public TileBitmap(Tile tile, string fileName, TracerParam tracerParam)
        {
            Tile = tile.Clone();
            FileName = (string)fileName.Clone();
            _tracerParam = tracerParam;

            Thread = new Thread(ThreadReadData);
            Thread.Start(this);
        }

        public override string ToString()
        {
            return "Bitmap " + Tile.ToString();
        }

        public void ClearData()
        {
            lock (Lock)
            {
                bitmapData = null;
                ShouldClearData = true;
            }
        }

        public byte[][] GetData()
        {
            byte[][] aucData = null;

            lock (Lock)
            {
                if (bitmapData != null)
                {
                    return (byte[][])bitmapData.Clone();
                }
            }

            DebugStatic.oDebugWatch.StartWatch("GetData");
            int nReTry = 1500;  // 15 seconds
            while (aucData == null && nReTry != 0)
            {
                Thread.Sleep(10);
                lock (Lock)
                {
                    if (bitmapData != null)
                    {
                        Thread = null;
                        aucData = (byte[][])bitmapData.Clone();
                    }
                    else
                    {
                        nReTry--;
                    }
                }
            }

            if (nReTry == 0)
            {
                try
                {
                    Thread.Abort();
                    Thread = null;
                }
                catch
                {
                }
            }
            DebugStatic.oDebugWatch.StopWatch("GetData");

            return aucData;
        }

        public void SetData(byte[][] aoData)
        {
            lock (Lock)
            {
                if (bitmapData == null && aoData != null)
                {
                    bitmapData = aoData;
                }
            }
            Thread.Sleep(10);
            lock (Lock)
            {
                if (ShouldClearData)
                {
                    bitmapData = null;
                }
            }
        }

        public static void ThreadReadData(object obj)
        {
            TileDownloader tileDownloader;
            TileBitmap tileBitmap = (TileBitmap)obj;
            TilePreparer preparer = new TilePreparer();
            byte[][] aoData = null;
            Bitmap bitmap = null;
            Stopwatch stopWatch;
            string tempString;

            stopWatch = new Stopwatch();
            stopWatch.Start();

            int nReTry = 3;
            while ((aoData == null) && (nReTry > 0) && (File.Exists(tileBitmap.FileName)))
            {
                try
                {
                    bitmap = new Bitmap(tileBitmap.FileName);
                    tileBitmap.GetColor(bitmap);
                    aoData = preparer.Prepare(bitmap, tileBitmap._tracerParam);
                    bitmap = null;
                }
                catch
                {
                    Thread.Sleep(10);
                    nReTry--;
                }
            }

            if (aoData == null)
            {
                tempString = "TileDownloader " + tileBitmap.Tile.ToString();
                DebugStatic.oDebugWatch.StartWatch(tempString);
                tileDownloader = new TileDownloader(tileBitmap._tracerParam.ServerEventArgs);
                bitmap = tileDownloader.Download(tileBitmap.Tile);
                DebugStatic.oDebugWatch.StopWatch(tempString);

                if (bitmap != null)
                {
                    tileBitmap.GetColor(bitmap);
                    aoData = preparer.Prepare(bitmap, tileBitmap._tracerParam);
                    tileBitmap.ShouldDownloadViaWms = true;
                }
            }

            tileBitmap.SetData(aoData);

            if (tileBitmap.ShouldDownloadViaWms)
            {
                tempString = "Bitmap.Save " + tileBitmap.Tile.ToString();
                DebugStatic.oDebugWatch.StartWatch(tempString);
                if (!File.Exists(tileBitmap.FileName))
                {
                    bitmap.Save(tileBitmap.FileName);
                }
                DebugStatic.oDebugWatch.StopWatch(tempString);
            }

            stopWatch.Stop();
            tileBitmap.TimeSpan = stopWatch.Elapsed;
            tileBitmap._tracerParam.AddNewTile(tileBitmap);
        }

        public void GetColor(Bitmap bitmap)
        {
            Point innerPointXY = _tracerParam.InnerPointXY;
            Tile tile = Tile;

            if (tile.FirstPointXY.X <= innerPointXY.X && tile.FirstPointXY.Y <= innerPointXY.Y)
            {
                int x = innerPointXY.X - tile.FirstPointXY.X;
                int y = innerPointXY.Y - tile.FirstPointXY.Y;

                if (x < tile.Resolution && y < tile.Resolution)
                {
                    y = (tile.Resolution - 1) - y;

                    Color oColor = bitmap.GetPixel(x, y);
                    _tracerParam.InnerColor = oColor;
                    //oBitmap.SetPixel(x, y, Color.White);
                    //oBitmap.Save(m_strFileName);
                }
            }
            return;
        }

    }

}
