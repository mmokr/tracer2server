﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System;
using System.Drawing;
using Tracer2Server.Trace;
using Tracer2Server.Projection;

namespace Tracer2Server.Tiles
{
    internal class Tiler
    {
        private readonly TracerParam _tracerParam;
        private readonly int _preLoadSize = 1;

        public Tiler(TracerParam tracerParam)
        {
            _tracerParam = tracerParam;

            TileSize = tracerParam.ServerEventArgs.TileSize;

            int i = 0;
            Resultion = tracerParam.ServerEventArgs.Resolution;
            Resultion >>= 1;
            while (Resultion != 0)
            {
                i++;
                Resultion >>= 1;
            }
            Resultion = 1 << i;
            ResolutionShift = i;

            _preLoadSize = Resultion > 1024 ? 1 : Resultion > 512 ? 2 : 3;
        }

        public double TileSize { get; } = 0.0002;
        public int Resultion { get; } = 1024;
        public int ResolutionShift { get; } = 10;

        public Tile[] GetTilesByTile(Tile tile)
        {
            int minX = (int)Math.Round(tile.Rectangle.Left / TileSize);
            int minY = (int)Math.Round(tile.Rectangle.Bottom / TileSize);

            Point firstPoint = tile.FirstPointXY;

            Tile[] tiles = new Tile[(_preLoadSize * 2 + 1) * (_preLoadSize * 2 + 1)];
            long yCount = 0;
            for (int y = -_preLoadSize; y <= _preLoadSize; y++)
            {
                for (int x = -_preLoadSize; x <= _preLoadSize; x++)
                {
                    Point point = new Point(
                        firstPoint.X + (x * Resultion),
                        firstPoint.Y + (y * Resultion)
                        );

                    if (point.X >= 0 && point.X <= _tracerParam.GeoMap.XYMax && point.X >= 0 && point.X <= _tracerParam.GeoMap.XYMax)
                    {
                        GeographicRectangle geoRectangle = new GeographicRectangle(
                            (minX + x) * TileSize,
                            (minY + y + 1) * TileSize,
                            (minX + x + 1) * TileSize,
                            (minY + y) * TileSize
                            );
                        tiles[yCount] = new Tile(geoRectangle, point, Resultion);
                    }
                    yCount++;
                }
            }
            return tiles;
        }

        public Point GetTileNrByPointGeo(GeographicPoint point)
        {
            int minX = (int)Math.Floor(point.Longitude / TileSize);
            int minY = (int)Math.Floor(point.Latitude / TileSize);

            return new Point(minX, minY);
        }
    }
}
