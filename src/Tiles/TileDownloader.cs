﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System;
using System.Globalization;
using System.Net;
using System.Drawing;
using System.IO;
using Tracer2Server.WebServer;
using System.Reflection;

namespace Tracer2Server.Tiles
{
    public class TileDownloader
    {
        private readonly string _url;
        private readonly int _skipBottom;

        public TileDownloader(ServerEventArgs serverEventArgs)
        {
            _skipBottom = 0;

            _url = serverEventArgs.Url;
            int startPosition = _url.IndexOf("wms:");
            if (startPosition == 0)
            {
                _url = _url.Remove(0, 4);
            }
            startPosition = _url.IndexOf("EPSG:");
            if (startPosition >= 0)
            {
                string strTemp = _url.Substring(startPosition, 9);
                _url = _url.Replace("SRS={proj(" + strTemp + ")}", "SRS=" + strTemp);
            }
            _url = _url.Replace("SRS={proj}", "SRS=EPSG:4326");

            _skipBottom = serverEventArgs.SkipBottom;
        }

        public Bitmap Download(Tile tile)
        {
            NumberFormatInfo defaultNumberFormat = NumberFormatInfo.InvariantInfo;
            double skipBottom = (double)_skipBottom / tile.Resolution * (tile.Rectangle.Top - tile.Rectangle.Bottom);

            string boundingBox = tile.Rectangle.Left.ToString("F4", defaultNumberFormat) + ","
                + tile.Rectangle.Bottom.ToString("F4", defaultNumberFormat) + ","
                + tile.Rectangle.Right.ToString("F4", defaultNumberFormat) + ","
                + (tile.Rectangle.Top + skipBottom).ToString("F4", defaultNumberFormat);
            string width = tile.Resolution.ToString();
            string height = (tile.Resolution + _skipBottom).ToString();

            string url = _url;
            url = url.Replace("{bbox}", boundingBox);
            url = url.Replace("{width}", width);
            url = url.Replace("{height}", height);

            using (WebClient wc = new WebClient())
            {
                try
                {
                    Version version = Assembly.GetCallingAssembly().GetName().Version;
                    wc.Headers.Add("user-agent", "Tracer2Server/" + version.Major.ToString() + "." + version.Minor.ToString());

                    byte[] data = wc.DownloadData(url);
                    using (Bitmap b = new Bitmap(tile.Resolution, tile.Resolution, System.Drawing.Imaging.PixelFormat.Format24bppRgb))
                    {
                        try
                        {
                            using (Graphics g = Graphics.FromImage(b))
                            {
                                using (MemoryStream ms = new MemoryStream(data))
                                {
                                    using (Bitmap tileBitmap = new Bitmap(ms))
                                    {
                                        g.DrawImage(tileBitmap, new Rectangle(new Point(), b.Size), new Rectangle(new Point(0, _skipBottom), b.Size), GraphicsUnit.Pixel);
                                    }
                                }
                            }
                            return (Bitmap)b.Clone();
                        }
                        catch
                        {
                            return null;
                        }
                    }
                }
                catch
                {
                    return null;
                }
            }
        }

    }

}
