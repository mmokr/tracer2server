﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System.Drawing;
using Tracer2Server.Projection;

namespace Tracer2Server.Tiles
{
    public class Tile
    {
        private readonly GeographicRectangle _rectangle;
        private Point FirstPoint;

        public GeographicRectangle Rectangle
        {
            get { return new GeographicRectangle(_rectangle.Left, _rectangle.Top, _rectangle.Right, _rectangle.Bottom); }
        }

        public Point FirstPointXY
        {
            get { return new Point(FirstPoint.X, FirstPoint.Y); }
        }

        public int Resolution { get; }

        public Tile(GeographicRectangle rectangle, Point firstPointXY, int resolution)
        {
            _rectangle = rectangle;
            FirstPoint = firstPointXY;
            Resolution = resolution;
        }

        public Tile Clone()
        {
            return new Tile(this.Rectangle, this.FirstPointXY, this.Resolution);
        }

        public override string ToString()
        {
            return _rectangle.ToString();
        }
    }
}
