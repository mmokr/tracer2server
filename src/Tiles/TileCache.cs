﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Tracer2Server.WebServer;
using Tracer2Server.Trace;

namespace Tracer2Server.Tiles
{
    internal class TileCache
    {
        private static readonly string CachePrefix;

        private readonly TracerParam _tracerParam;
        private readonly string _cachePath;
        private readonly List<TileBitmap> _tileBitmaps = new List<TileBitmap>();
        private readonly object _lock = new object();

        static TileCache()
        {
            if (OperationSystem.isWindows)
            {
                CachePrefix = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + Path.DirectorySeparatorChar + "Tracer2Server" + Path.DirectorySeparatorChar + "cache" + Path.DirectorySeparatorChar;
            }
            else
            {
                CachePrefix = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + Path.DirectorySeparatorChar + "Tracer2Server" + Path.DirectorySeparatorChar + "cache" + Path.DirectorySeparatorChar;
            }

            DirectoryInfo cacheDirectory = new DirectoryInfo(CachePrefix);
            try
            {
                if (cacheDirectory.Exists)
                {
                    foreach (DirectoryInfo di in cacheDirectory.GetDirectories())
                    {
                        DeleteDirectory(di);
                    }
                }
                else
                {
                    cacheDirectory.Create();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
        }

        private static bool DeleteDirectory(DirectoryInfo directory)
        {
            bool directoryDeleted = true;

            try
            {
                foreach (DirectoryInfo subDirectory in directory.GetDirectories())
                {
                    if (!DeleteDirectory(subDirectory))
                    {
                        directoryDeleted = false;
                    }
                }
                foreach (FileInfo file in directory.GetFiles())
                {
                    if (file.CreationTime < DateTime.Now.AddMonths(-1))
                    {
                        file.Delete();
                    }
                    else
                    {
                        directoryDeleted = false;
                    }
                }
                if (directoryDeleted)
                {
                    directory.Delete(false);
                }
            }
            catch (Exception)
            {
                return false;
            }
            return directoryDeleted;
        }

        public TileCache(TracerParam tracerParam)
        {
            _tracerParam = tracerParam;

            ServerEventArgs serverEventArgs = tracerParam.ServerEventArgs;
            int hashCode = (serverEventArgs.Url + serverEventArgs.TileSize + serverEventArgs.Resolution).GetHashCode();
            string path = GetValidFileName(hashCode.ToString("X"));
            _cachePath = CachePrefix + path + Path.DirectorySeparatorChar;
            Directory.CreateDirectory(_cachePath);
        }

        public List<TileBitmap> TileBitmaps
        {
            get
            {
                List<TileBitmap> bitmaps = new List<TileBitmap>();
                lock (_lock)
                {
                    foreach (TileBitmap tileBitmap in _tileBitmaps)
                    {
                        bitmaps.Add(tileBitmap);
                    }
                }
                return bitmaps;
            }
        }

        public void ClearAllData()
        {
            lock (_lock)
            {
                foreach (TileBitmap tileBitmaps in _tileBitmaps)
                {
                    tileBitmaps.ClearData();
                }
            }
        }

        public string GetValidFileName(string strFilename)
        {
            var invalidChars = Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            var invalidReStr = string.Format("[{0}]+", invalidChars);

            var reservedWords = new[] {
                "CON", "PRN", "AUX", "CLOCK$", "NUL", "COM0", "COM1", "COM2", "COM3", "COM4",
                "COM5", "COM6", "COM7", "COM8", "COM9", "LPT0", "LPT1", "LPT2", "LPT3", "LPT4",
                "LPT5", "LPT6", "LPT7", "LPT8", "LPT9"
            };

            var sanitisedNamePart = Regex.Replace(strFilename, invalidReStr, "_");
            foreach (var reservedWord in reservedWords)
            {
                var reservedWordPattern = string.Format("^{0}\\.", reservedWord);
                sanitisedNamePart = Regex.Replace(sanitisedNamePart, reservedWordPattern, "_reservedWord_.", RegexOptions.IgnoreCase);
            }
            return sanitisedNamePart;
        }

        private string GetFileName(Tile tile)
        {
            NumberFormatInfo defaultNumberFormat = NumberFormatInfo.InvariantInfo;
            return _cachePath +
                tile.Rectangle.Left.ToString("F4", defaultNumberFormat) + "_" +
                tile.Rectangle.Bottom.ToString("F4", defaultNumberFormat) + "_" +
                tile.Rectangle.Right.ToString("F4", defaultNumberFormat) + "_" +
                tile.Rectangle.Top.ToString("F4", defaultNumberFormat) + ".BMP";
        }

        private TileBitmap GetCacheBitmap(Tile tile)
        {
            string fileName = GetFileName(tile);
            TileBitmap result = null;

            lock (_lock)
            {
                foreach (TileBitmap tileBitmap in _tileBitmaps)
                {
                    if (tileBitmap.FileName.Equals(fileName))
                    {
                        result = tileBitmap;
                        break;
                    }
                }
            }
            return result;
        }

        public byte[][] GetTile(Tile inputTile)
        {
            TileBitmap newTileBitmap;

            lock (_lock)
            {
                Tile[] tiles = _tracerParam.Tiler.GetTilesByTile(inputTile);

                if (GetCacheBitmap(inputTile) == null)
                {
                    newTileBitmap = new TileBitmap(inputTile, GetFileName(inputTile), _tracerParam);
                    _tileBitmaps.Add(newTileBitmap);
                }
                foreach (Tile tile in tiles)
                {
                    if (tile != null && GetCacheBitmap(tile) == null)
                    {
                        Thread.Sleep(10);
                        newTileBitmap = new TileBitmap(tile, GetFileName(tile), _tracerParam);
                        _tileBitmaps.Add(newTileBitmap);
                    }
                }
            }
            byte[][] bitmapData = GetCacheBitmap(inputTile).GetData();
            if (bitmapData == null)
            {
                throw new Exception("Can't load Tile: " + inputTile.ToString());
            }
            return bitmapData;
        }
    }
}
