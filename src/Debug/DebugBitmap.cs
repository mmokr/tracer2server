﻿/**
 *  Tracer2Server - Server for the JOSM plugin Tracer2
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

using System;
using System.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using Tracer2Server.Trace;
using System.IO;

namespace Tracer2Server.Debug
{
    internal class DebugBitmap
    {
        const byte BACKGROUND = 0;
        const byte PEN = 1;
        const byte OBJECT = 2;
        const byte BORDER = 3;
        const byte TRACK = 4;

        private readonly Color[] Colors = { Color.White, Color.LightBlue, Color.LightGreen, Color.Yellow, Color.Blue };
        private Bitmap Bitmap;
        private Rectangle BitmapArea;

        private const string DebugPrefix = "debug//";

        public GeoMap GeoMap { get; set; }

        public DebugBitmap()
        {
            if (DebugStatic.bDebugBitmap)
            {
                DirectoryInfo diCache = new DirectoryInfo(DebugPrefix);
                try
                {
                    if (!diCache.Exists)
                    {
                        diCache.Create();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("The process failed: {0}", e.ToString());
                }
                finally { }
            }
        }

        private void SetBitmapArea(Rectangle shapeArea)
        {
            int xmin = shapeArea.Left - 50;
            int xmax = shapeArea.Right + 50;
            int ymin = PosY2Bitmap(shapeArea.Bottom) - 50;
            int ymax = PosY2Bitmap(shapeArea.Top) + 50;

            if (xmin < 0)
            {
                xmin = 0;
            }
            if (xmax > GeoMap.XYMax)
            {
                xmax = GeoMap.XYMax;
            }
            if (ymin < 0)
            {
                ymin = 0;
            }
            if (ymax > GeoMap.XYMax)
            {
                ymax = GeoMap.XYMax;
            }
            BitmapArea = new Rectangle(xmin, ymin, xmax - xmin, ymax - ymin);
        }

        public void MakeBitmap(Point innerPointXY, Rectangle shapeArea)
        {
            int x;
            int y;
            byte ucData;

            if (!DebugStatic.bDebugBitmap) return;

            SetBitmapArea(shapeArea);

            try
            {
                Bitmap = new Bitmap(BitmapArea.Width, BitmapArea.Height, PixelFormat.Format32bppArgb);

                for (x = 0; x < BitmapArea.Width; x++)
                {
                    for (y = 0; y < BitmapArea.Height; y++)
                    {
                        ucData = GeoMap.GetPix(x + BitmapArea.Left, PosY2Bitmap(y + BitmapArea.Top));
                        if (ucData < 5)
                        {
                            Bitmap.SetPixel(x, y, Colors[ucData]);
                        }
                        else
                        {
                            Bitmap.SetPixel(x, y, Color.Black);
                        }
                    }
                }
                try
                {
                    Bitmap.SetPixel(innerPointXY.X - BitmapArea.Left, PosY2Bitmap(innerPointXY.Y) - BitmapArea.Top, Color.Black);
                }
                catch //( Exception e )
                {
                    // Start Pos in not inside of Shape
                }
                Bitmap.Save(DebugPrefix + "Base.bmp");
            }
            catch
            {
            }
        }

        public void MakeBitmap(string name, BorderPart[] borderLines)
        {
            if (!DebugStatic.bDebugBitmap) return;

            Bitmap newBitmap = (Bitmap)Bitmap.Clone();
            Color gray = Color.FromArgb(255, 128, 128, 128);
            try
            {
                foreach (BorderPartLine l in borderLines.ToArray())
                {
                    if (newBitmap.GetPixel(l.m_oPointStart.X - BitmapArea.Left, PosY2Bitmap(l.m_oPointStart.Y) - BitmapArea.Top).Equals(gray))
                    {
                        newBitmap.SetPixel(l.m_oPointStart.X - BitmapArea.Left, PosY2Bitmap(l.m_oPointStart.Y) - BitmapArea.Top, Color.Black);
                    }
                    else
                    {
                        newBitmap.SetPixel(l.m_oPointStart.X - BitmapArea.Left, PosY2Bitmap(l.m_oPointStart.Y) - BitmapArea.Top, gray);
                    }

                    if (newBitmap.GetPixel(l.m_oPointEnd.X - BitmapArea.Left, PosY2Bitmap(l.m_oPointEnd.Y) - BitmapArea.Top).Equals(gray))
                    {
                        newBitmap.SetPixel(l.m_oPointEnd.X - BitmapArea.Left, PosY2Bitmap(l.m_oPointEnd.Y) - BitmapArea.Top, Color.Black);
                    }
                    else
                    {
                        newBitmap.SetPixel(l.m_oPointEnd.X - BitmapArea.Left, PosY2Bitmap(l.m_oPointEnd.Y) - BitmapArea.Top, gray);
                    }
                }
                newBitmap.Save(DebugPrefix + name + ".bmp");
            }
            catch
            {
            }
        }

        private int PosY2Bitmap(int y)
        {
            return GeoMap.XYMax - y;
        }
    }
}
